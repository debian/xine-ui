# translation of it.po to Italian
# This file is distributed under the same license as the PACKAGE package.
# Copyright (C) 2007 THE PACKAGE'S COPYRIGHT HOLDER.
#
# Giovanni Venturi <jumpyj@libero.it>, 2003.
# Giovanni Venturi <jumpyj@tiscali.it>, 2003.
# Diego 'Flameeyes' Pettenò <flameeyes@gentoo.org>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: it\n"
"Report-Msgid-Bugs-To: xine-devel@lists.sourceforge.net\n"
"POT-Creation-Date: 2021-12-12 21:56+0200\n"
"PO-Revision-Date: 2007-04-23 23:05+0200\n"
"Last-Translator: Diego 'Flameeyes' Pettenò <flameeyes@gentoo.org>\n"
"Language-Team: Italian\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.2\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#, c-format
msgid "Unable to initialize Imlib\n"
msgstr ""

#, c-format
msgid "Cannot open display\n"
msgstr ""

#, c-format
msgid "Warning! Synchronized X activated - this is very slow...\n"
msgstr ""

#, c-format
msgid ""
"\n"
"XInitThreads failed - looks like you don't have a thread-safe xlib.\n"
msgstr ""

msgid "..."
msgstr ""

msgid "More sources..."
msgstr ""

msgid "OK"
msgstr "OK"

msgid "No"
msgstr "No"

msgid "Yes"
msgstr "Sì"

msgid "Cancel"
msgstr "Annulla"

msgid "Question?"
msgstr "Domande?"

msgid "Select current entry"
msgstr "Seleziona l'elemento corrente"

msgid "Close MRL browser window"
msgstr "Chiudi la finestra di navigazione MRL"

msgid "Play selected entry"
msgstr "Riproduci l'elemento selezionato"

msgid "Information"
msgstr "Informazioni"

msgid "Warning"
msgstr ""

msgid "Error"
msgstr "Errore"

#, c-format
msgid "gui_main: selected visual %#lx does not exist, trying default visual\n"
msgstr ""

#, c-format
msgid "gui_main: couldn't find true color visual.\n"
msgstr ""

#, c-format
msgid "XF86VidMode Extension (%d.%d) detected, trying to use it.\n"
msgstr ""

#, c-format
msgid "XF86VidMode Extension: %d modelines found.\n"
msgstr ""

#, c-format
msgid "XF86VidModeModeLine %dx%d isn't valid: discarded.\n"
msgstr ""

#, c-format
msgid ""
"XF86VidMode Extension: could not get list of available modelines. Failed.\n"
msgstr ""

#, c-format
msgid "XF86VidMode Extension: initialization failed, not using it.\n"
msgstr ""
